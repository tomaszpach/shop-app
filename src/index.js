import React from 'react'
import {render} from 'react-dom'
import App from './components/App'

import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import getMuiTheme from 'material-ui/styles/getMuiTheme';

const muiTheme = getMuiTheme({
    fontFamily: '\'Comfortaa\', cursive',
    palette: {
        primary1Color: '#85bb65'
    }
});

render(
    <MuiThemeProvider muiTheme={muiTheme}>
        <App/>
    </MuiThemeProvider>,
    document.getElementById('root')
);