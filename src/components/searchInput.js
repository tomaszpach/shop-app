import React from 'react';

const searchInput = ({filterList}) => (
    <input id='search-input' type='search' placeholder='Szukaj...' onInput={(e) => filterList(e)}/>
);

export default searchInput;