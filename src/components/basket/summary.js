import React from 'react';

const summary = ({summary}) => (
    <div>
        <p>Podsumowanie: {summary} zł</p>
    </div>
);

export default summary;