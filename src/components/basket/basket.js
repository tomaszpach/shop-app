import React from 'react';
import Item from './item';
import Summary from './summary';

import {List} from 'material-ui/List';

const basket = ({items, removeFromBasket}) => {
    let basketItems = [],
        summary = 0;

    if (items.length > 0) {
        basketItems = items.map((item, i) => {
                summary = summary + item.price;

                return (
                    <Item key={i} item={item} removeFromBasket={() => removeFromBasket(item)}/>
                )
            }
        )
    }

    return (
        <aside id='basket'>
            <h2>Twój koszyk</h2>
            <List className={basketItems.length > 0 ? 'items' : 'empty'}>
                {basketItems}
            </List>

            {basketItems.length > 0 ? <Summary summary={summary}/> : <div>Twój koszyk jest pusty</div>}
        </aside>
    )
};

export default basket;