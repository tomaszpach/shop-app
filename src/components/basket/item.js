import React from 'react';

import Avatar from 'material-ui/Avatar';
import {ListItem} from 'material-ui/List';
import DeleteForeverIcon from 'material-ui/svg-icons/action/delete-forever';

const item = ({item, removeFromBasket}) => (
    <ListItem
        primaryText={item.name}
        leftAvatar={<Avatar src={item.photo}/>}
        rightIcon={<DeleteForeverIcon/>}
        onClick={() => removeFromBasket()}
    />
);

export default item;