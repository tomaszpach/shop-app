import React from 'react';
import RaisedButton from 'material-ui/RaisedButton';

const item = ({item, addToBasket}) => {
    let isInStock = item.in_stock ? '- dostępne w magazynie' : '- niedostępne w magazynie';
    return (
        <div className='item'>
            <img src={item.photo} alt={item.name}/>
            <h2 className='name'>{item.name}</h2>
            <div className='price'>{item.price} zł {isInStock}</div>
            <div className='description'>{item.description}</div>
            <RaisedButton className='add-to-cart-button' label="Dodaj do koszyka" onClick={() => addToBasket(item)} />
        </div>
    )
};

export default item;