import React from 'react';
import Item from './item';

const itemsList = ({items, addToBasket}) => {
    let listItems = [];

    if (items.length > 0) {
        listItems = items.map(item =>
            <Item key={item.id} item={item} addToBasket={() => addToBasket(item)}/>
        )
    }

    return (
        <div id='items-list'>
            {listItems}
        </div>
    )
};

export default itemsList;