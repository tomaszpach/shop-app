import React from 'react'

import SearchInput from './searchInput'
import ItemsList from './itemsList/itemsList'
import Basket from './basket/basket';
import CircularProgress from 'material-ui/CircularProgress';
import Sort from 'material-ui/svg-icons/content/sort';
import Snackbar from "material-ui/Snackbar/index";

class App extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            products: {},
            basket: [],
            loading: true,
            itemsToDisplay: [],
            sortBy: '',
            newAddedItem: '',
            snackbarOpen: false
        }
    }

    componentWillMount() {
        this.fetchData();
    }

    fetchData() {
        this.setState({loading: true});
        fetch('http://private-1c19e-reactlesson.apiary-mock.com/products')
            .then(response => response.json())
            .then(data => this.setState({
                products: data,
                itemsToDisplay: data,
            }))
            .finally(() => this.setState({loading: false}));
    }

    addToBasket(item) {
        this.setState({basket: [...this.state.basket, item], newAddedItem: item.name, snackbarOpen: true})
    }

    removeFromBasket(item) {
        this.setState({basket: this.state.basket.filter(element => element !== item)})
    }

    filterList(event) {
        let updatedList = this.state.products;
        updatedList = updatedList.filter(function (item) {
            return item.name.toLowerCase().search(
                event.target.value.toLowerCase()) !== -1;
        });
        this.setState({itemsToDisplay: updatedList});
    }

    snackbarClose() {
        this.setState({
            snackbarOpen: false,
            newAddedItem: ''
        })
    }

    sortItems(e, sortBy = 'down') {
        const compare = (a, b) => {
            if (a.price < b.price)
                return sortBy === 'down' ? -1 : 1;
            if (a.price > b.price)
                return sortBy === 'down' ? 1 : -1;
            return 0;
        };

        let itemsToDisplay = this.state.itemsToDisplay;
        itemsToDisplay = itemsToDisplay.sort(compare);

        this.setState({itemsToDisplay, sortBy});
    }

    render() {
        return (
            <div id='app'>
                <SearchInput filterList={(e) => this.filterList(e)}/>
                <div id='sort-by'>
                    <h4>Sortuj według ceny:</h4>
                    <Sort onClick={(e) => this.sortItems(e, 'down')}
                          className={this.state.sortBy === 'down' ? 'active' : null}/>
                    <Sort onClick={(e) => this.sortItems(e, 'up')}
                          className={this.state.sortBy === 'up' ? 'active' : null}/>
                </div>
                <main>
                    {this.state.loading ? <CircularProgress className='circular-progress' size={120} thickness={15}/> :
                        <ItemsList items={this.state.itemsToDisplay} addToBasket={(item) => this.addToBasket(item)}/>}
                    <Snackbar
                        className='snackbar'
                        open={this.state.snackbarOpen}
                        message={'Dodano: ' + this.state.newAddedItem}
                        autoHideDuration={4000}
                        onRequestClose={() => this.snackbarClose()}
                    />
                    <Basket items={this.state.basket} removeFromBasket={(item) => this.removeFromBasket(item)}/>
                </main>
            </div>
        )
    }
}

export default App;